/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.shapeframe;

/**
 *
 * @author ADMIN
 */
public class Rectangle extends Shape{
    private double wide;
    private double length;

    public Rectangle(double wide, double length) {
        super("Rectangle");
        this.length = length;
        this.wide = wide;
    }

    public double getWide() {
        return wide;
    }

    public void setWide(double wide) {
        this.wide = wide;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
    

    @Override
    public double calArea() {
       return wide*length;
    }

    @Override
    public double calPerimeter() {
        return 2*(wide+length);
    }
    
}
