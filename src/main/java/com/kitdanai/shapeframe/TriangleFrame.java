/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ADMIN
 */
public class TriangleFrame {
    public static void main(String[] args) {
         JFrame frame = new JFrame("triangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblBase = new JLabel("base:",JLabel.TRAILING);
        lblBase.setSize(50,20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);
        
         JLabel lblHigh = new JLabel("high:",JLabel.TRAILING);
        lblHigh.setSize(50,20);
        lblHigh.setLocation(5, 40);
        lblHigh.setBackground(Color.WHITE);
        lblHigh.setOpaque(true);
        frame.add(lblHigh);
        
        JTextField txtBase = new JTextField();
        txtBase.setSize(50,20);
        txtBase.setLocation(60,5);
        frame.add(txtBase);
        
        JTextField txtHigh = new JTextField();
        txtHigh.setSize(50,20);
        txtHigh.setLocation(60,40);
        frame.add(txtHigh);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120,5);
        frame.add(btnCalculate);
        
        JLabel lblResult = new JLabel("triangle base= ?? high=?? area= ?? perimeter= ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
             String strBase = txtBase.getText();
             String strHigh = txtHigh.getText();
             double base = Double.parseDouble(strBase);
             double high = Double.parseDouble(strHigh);
             Triangle triangle = new Triangle(base,high);
             lblResult.setText("Triangle b = "+ String.format("%.2f", triangle.getBase())
                     +"Triangle h = "+ String.format("%.2f", triangle.getHigh())+"area= "
                     + String.format("%.2f", triangle.calArea())+"perimeter= "+ String.format("%.2f", triangle.calPerimeter()));
            }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame,
                    "Error: Please input number","Error",JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                    txtHigh.setText("");
                    txtHigh.requestFocus();
           }
          }       
            
    });
        
        
        frame.setVisible(true);
    }
}
