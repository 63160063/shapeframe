/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ADMIN
 */
public class RectangleFrame {
    public static void main(String[] args) {
         JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblWide = new JLabel("Wide:",JLabel.TRAILING);
        lblWide.setSize(50,20);
        lblWide.setLocation(5, 5);
        lblWide.setBackground(Color.WHITE);
        lblWide.setOpaque(true);
        frame.add(lblWide);
        
         JLabel lblLength = new JLabel("length:",JLabel.TRAILING);
        lblLength.setSize(50,20);
        lblLength.setLocation(5, 40);
        lblLength.setBackground(Color.WHITE);
        lblLength.setOpaque(true);
        frame.add(lblLength);
        
        JTextField txtWide = new JTextField();
        txtWide.setSize(50,20);
        txtWide.setLocation(60,5);
        frame.add(txtWide);
        
        JTextField txtLength = new JTextField();
        txtLength.setSize(50,20);
        txtLength.setLocation(60,40);
        frame.add(txtLength);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120,5);
        frame.add(btnCalculate);
        
        JLabel lblResult = new JLabel("Rectangle wide= ?? length=?? area= ?? perimeter= ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
             String strWide = txtWide.getText();
             String strLength = txtLength.getText();
             double wide = Double.parseDouble(strWide);
             double length = Double.parseDouble(strLength);
             Rectangle rectangle = new Rectangle(wide,length);
             lblResult.setText("Rectangle w = "+ String.format("%.2f", rectangle.getWide())
                     +"Rectangle l = "+ String.format("%.2f", rectangle.getLength())+"area= "
                     + String.format("%.2f", rectangle.calArea())+"perimeter= "+ String.format("%.2f", rectangle.calPerimeter()));
            }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame,
                    "Error: Please input number","Error",JOptionPane.ERROR_MESSAGE);
                    txtWide.setText("");
                    txtWide.requestFocus();
                    txtLength.setText("");
                    txtLength.requestFocus();
           }
          }       
            
    });
        
        
        frame.setVisible(true);
    }
}
